from tkinter import *


def calculate_cell_number(coord_x, coord_y):
    return int((coord_x - indent_x) // size), int((coord_y - indent_y) // size)


def handle_click_cell(event):
    global clicked_cell_number, clicked_cell, clicked_cell_initial_color
    clicked_cell_number = calculate_cell_number(event.x, event.y)
    x, y = clicked_cell_number
    clicked_cell = list_of_cells[x][y]
    clicked_cell_initial_color = canvas.itemcget(clicked_cell, 'fill')
    if clicked_cell_initial_color == 'white' or clicked_cell_initial_color == '#eee':
        canvas.itemconfig(clicked_cell, fill='yellow')


def handle_unclick_cell(event):
    global current_player, clicked_cell_number, clicked_cell, clicked_cell_initial_color
    unclick_cell_number = calculate_cell_number(event.x, event.y)
    color = canvas.itemcget(clicked_cell, 'fill')
    x = indent_x + clicked_cell_number[0] * size
    y = indent_y + clicked_cell_number[1] * size
    if unclick_cell_number == clicked_cell_number and color == 'yellow':
        canvas.itemconfig(clicked_cell, fill=clicked_cell_initial_color)
        canvas.create_image(x, y, image=cross if current_player == 'blue' else zero, anchor=NW)
        # Checking if there appeared 5 in a row:
        check_if_five(x + size/2, y + size/2, current_player)
        update_score()
        # Changing next player:
        current_player = 'blue' if current_player == 'red' else 'red'
        canvas.config(bg='#fcc' if canvas.cget('bg') == '#ccf' else '#ccf')

    else:
        canvas.itemconfig(clicked_cell, fill=clicked_cell_initial_color)


def check_what_in_cell(coord_x, coord_y):
    image_index = canvas.find_overlapping(coord_x, coord_y, coord_x, coord_y)[-1]
    if image_index > rows * columns:
        return 'zero' if image_index % 2 == 0 else 'cross'
    else:
        return None


def check_if_five(coord_x, coord_y, current_player):
    check_row(coord_x, coord_y, current_player)
    check_row(coord_x, coord_y, current_player)
    check_column(coord_x, coord_y, current_player)
    check_column(coord_x, coord_y, current_player)
    check_diag_00(coord_x, coord_y, current_player)
    check_diag_00(coord_x, coord_y, current_player)
    check_diag_01(coord_x, coord_y, current_player)
    check_diag_01(coord_x, coord_y, current_player)


def check_row(coord_x, coord_y, current_player):
    global blue_score, red_score
    item_to_check = 'cross' if current_player == 'blue' else 'zero'
    counter = 1
    cells_to_change_list = [calculate_cell_number(coord_x, coord_y)]
    x = coord_x - size
    y = coord_y

    while counter < 5 and x > indent_x:
        num_x, num_y = calculate_cell_number(x, y)
        if check_what_in_cell(x, y) == item_to_check and row_fives_list[num_x][num_y] == 0:
            counter += 1
            cells_to_change_list.insert(0, calculate_cell_number(x, y))
        else:
            break
        x -= size

    x = coord_x + size

    while counter < 5 and x < indent_x + (rows * size):
        num_x, num_y = calculate_cell_number(x, y)
        if check_what_in_cell(x, y) == item_to_check and row_fives_list[num_x][num_y] == 0:
            counter += 1
            cells_to_change_list.append(calculate_cell_number(x, y))
        else:
            break
        x += size

    color = '#afa'
    if counter == 5:
        if current_player == 'blue':
            blue_score += 1
        else:
            red_score += 1
        for i in range(counter):
            item = cells_to_change_list[i]
            cell_color = canvas.itemcget(list_of_cells[item[0]][item[1]], 'fill')
            new_color = '#4b4' if cell_color == color or cell_color == '#4b4' else color
            canvas.itemconfig(list_of_cells[item[0]][item[1]], fill=new_color)
            if i in range(1, 4):
                row_fives_list[item[0]][item[1]] = 1
        return True
    else:
        return False


def check_column(coord_x, coord_y, current_player):
    global blue_score, red_score
    item_to_check = 'cross' if current_player == 'blue' else 'zero'
    counter = 1
    cells_to_change_list = [calculate_cell_number(coord_x, coord_y)]
    x = coord_x
    y = coord_y - size

    while counter < 5 and y > indent_y:
        num_x, num_y = calculate_cell_number(x, y)
        if check_what_in_cell(x, y) == item_to_check and column_fives_list[num_x][num_y] == 0:
            counter += 1
            cells_to_change_list.insert(0, calculate_cell_number(x, y))
        else:
            break
        y -= size

    y = coord_y + size

    while counter < 5 and y < indent_y + (columns * size):
        num_x, num_y = calculate_cell_number(x, y)
        if check_what_in_cell(x, y) == item_to_check and column_fives_list[num_x][num_y] == 0:
            counter += 1
            cells_to_change_list.append(calculate_cell_number(x, y))
        else:
            break
        y += size

    color = '#afa'
    if counter == 5:
        if current_player == 'blue':
            blue_score += 1
        else:
            red_score += 1
        for i in range(counter):
            item = cells_to_change_list[i]
            cell_color = canvas.itemcget(list_of_cells[item[0]][item[1]], 'fill')
            new_color = '#4b4' if cell_color == color or cell_color == '#4b4' else color
            canvas.itemconfig(list_of_cells[item[0]][item[1]], fill=new_color)
            if i in range(1, 4):
                column_fives_list[item[0]][item[1]] = 1
        return True
    else:
        return False


def check_diag_00(coord_x, coord_y, current_player):
    global blue_score, red_score
    item_to_check = 'cross' if current_player == 'blue' else 'zero'
    counter = 1
    cells_to_change_list = [calculate_cell_number(coord_x, coord_y)]
    x = coord_x - size
    y = coord_y - size

    while counter < 5 and x > indent_x and y > indent_y:
        num_x, num_y = calculate_cell_number(x, y)
        if check_what_in_cell(x, y) == item_to_check and diag00_fives_list[num_x][num_y] == 0:
            counter += 1
            cells_to_change_list.insert(0, calculate_cell_number(x, y))
        else:
            break
        x -= size
        y -= size

    x = coord_x + size
    y = coord_y + size

    while counter < 5 and x < indent_x + (rows * size) and y < indent_y + (columns * size):
        num_x, num_y = calculate_cell_number(x, y)
        if check_what_in_cell(x, y) == item_to_check and diag00_fives_list[num_x][num_y] == 0:
            counter += 1
            cells_to_change_list.append(calculate_cell_number(x, y))
        else:
            break
        x += size
        y += size

    color = '#afa'
    if counter == 5:
        if current_player == 'blue':
            blue_score += 1
        else:
            red_score += 1
        for i in range(counter):
            item = cells_to_change_list[i]
            cell_color = canvas.itemcget(list_of_cells[item[0]][item[1]], 'fill')
            new_color = '#4b4' if cell_color == color or cell_color == '#4b4' else color
            canvas.itemconfig(list_of_cells[item[0]][item[1]], fill=new_color)
            if i in range(1, 4):
                diag00_fives_list[item[0]][item[1]] = 1
        return True
    else:
        return False


def check_diag_01(coord_x, coord_y, current_player):
    global blue_score, red_score
    item_to_check = 'cross' if current_player == 'blue' else 'zero'
    counter = 1
    cells_to_change_list = [calculate_cell_number(coord_x, coord_y)]
    x = coord_x - size
    y = coord_y + size

    while counter < 5 and x > indent_x and y < indent_y + (columns * size):
        num_x, num_y = calculate_cell_number(x, y)
        if check_what_in_cell(x, y) == item_to_check and diag01_fives_list[num_x][num_y] == 0:
            counter += 1
            cells_to_change_list.insert(0, calculate_cell_number(x, y))
        else:
            break
        x -= size
        y += size

    x = coord_x + size
    y = coord_y - size

    while counter < 5 and x < indent_x + (rows * size) and y > indent_y:
        num_x, num_y = calculate_cell_number(x, y)
        if check_what_in_cell(x, y) == item_to_check and diag01_fives_list[num_x][num_y] == 0:
            counter += 1
            cells_to_change_list.append(calculate_cell_number(x, y))
        else:
            break
        x += size
        y -= size

    color = '#afa'
    if counter == 5:
        if current_player == 'blue':
            blue_score += 1
        else:
            red_score += 1
        for i in range(counter):
            item = cells_to_change_list[i]
            cell_color = canvas.itemcget(list_of_cells[item[0]][item[1]], 'fill')
            new_color = '#4b4' if cell_color == color or cell_color == '#4b4' else color
            canvas.itemconfig(list_of_cells[item[0]][item[1]], fill=new_color)
            if i in range(1, 4):
                diag01_fives_list[item[0]][item[1]] = 1
        return True
    else:
        return False


def update_score():
    global blue_score, red_score
    canvas.itemconfig(blue_score_text, text=blue_score)
    canvas.itemconfig(red_score_text, text=red_score)


game_window = Tk()
game_window.title("Game")

rows = 15
columns = 15
size = 40

canvas = Canvas(game_window, width=rows * size + 100, height=columns * size + 150, bg='#ccf')
canvas.pack()


cross = PhotoImage(file='Cross.gif')
zero = PhotoImage(file='Zero.gif')
current_player = 'blue'
clicked_cell_initial_color = None
clicked_cell_number = None
clicked_cell = None


list_of_cells = []
row_of_cells = []
indent_x = 50
indent_y = 100

row_fives_list = []
column_fives_list = []
diag00_fives_list = []
diag01_fives_list = []

# Creating lists containing info about existing fives
for row in range(rows):
    row_fives_list.append([])
    column_fives_list.append([])
    diag00_fives_list.append([])
    diag01_fives_list.append([])
    for col in range(columns):
        row_fives_list[row] += [0]
        column_fives_list[row] += [0]
        diag00_fives_list[row] += [0]
        diag01_fives_list[row] += [0]

# Creating cells
for row in range(rows):
    list_of_cells.append([])
    for col in range(columns):
        x1 = indent_x + row * size
        y1 = indent_y + col * size
        x2 = x1 + size
        y2 = y1 + size
        list_of_cells[row].append(
            canvas.create_rectangle(x1, y1, x2, y2,
                                    fill='white'
                                           if (x1 + y1 - indent_x - indent_y) % (size * 2) == 0
                                           else '#eee')
            )

        canvas.tag_bind(list_of_cells[row][col], "<Button-1>", handle_click_cell)
        canvas.tag_bind(list_of_cells[row][col], "<ButtonRelease-1>", handle_unclick_cell)

blue_score = 0
red_score = 0
middle = canvas.winfo_reqwidth() / 2
canvas.create_text(middle, 50, text=':', font='Roboto, 50')
blue_score_text = canvas.create_text(middle - 20, 53, font='Roboto, 50', text=blue_score, anchor='e', tags='blue_score', fill='blue')
red_score_text = canvas.create_text(middle + 20, 53, font='Roboto, 50', text=red_score, anchor='w', tags='blue_score', fill='red')


game_window.mainloop()
